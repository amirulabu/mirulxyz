---
layout: layouts/post.njk
title: Projects
templateClass: tmpl-post
date: 2022-01-02
eleventyNavigation:
  key: Projects
  order: 4
---
# 2021

## Digital Asset Management

Web application for managing assets for an engineering design solution company. Among features developed are
- Full authentication and authorization with multiple access roles to selected features
- CRUD documents from AWS S3 in browser
- Connect with Autodesk Forge for viewing 3D models
- Setting up full CI/CD using combination of AWS services


## Go Print Sticker
Landing page for collecting leads, every lead is sent to telegram for quick customer follow up. Using serverless framework
<a href="https://goprintsticker.com/" target="_blank">Link</a>

<a href="/img/ScreenshotGoPrintSticker.png" target="_blank">
  <div class="web-screenshot">
    <img src="/img/ScreenshotGoPrintSticker.png" />
  </div>
</a>

# 2020

## Faizal Property
Real estate agent landing page, and custom CRM. Built using NuxtJS, Strapi CMS and MongoDB and deployed on AWS Lightsail.
<a href="
https://faizalproperty.com/" target="_blank">Link</a>

<a href="/img/ScreenshotFaizalProperty.png" target="_blank">
  <div class="web-screenshot">
    <img src="/img/ScreenshotFaizalProperty.png" />
  </div>
</a>

# 2019

## Homestay Teratak Melati
Static site website with JavaScript for better user experience.
<a href="https://ayerkerohhomestay.com/" target="_blank">Link</a>

<a href="/img/ScreenshotHomestayTeratakMelati.png" target="_blank">
  <div class="web-screenshot">
    <img src="/img/ScreenshotHomestayTeratakMelati.png" />
  </div>
</a>

## Bedhills Homestay
Static site website with JavaScript for better user experience.
<a href="https://homestaybukitkatil.com/" target="_blank">Link</a>

<a href="/img/ScreenshotBedhillsHomestay.png" target="_blank">
  <div class="web-screenshot">
    <img src="/img/ScreenshotBedhillsHomestay.png" />
  </div>
</a>


## Belajar Python Bot
A Telegram bot that asks some questions on Python. If the user answer correctly, admin Belajar Python Telegram Group will be notified.
<a href="https://telegram.me/belajarpythonbot" target="_blank">Telegram Bot</a>

<a href="/img/ScreenshotBelajarPythonBot.png" target="_blank">
  <div class="web-screenshot">
    <img src="/img/ScreenshotBelajarPythonBot.png" />
  </div>
</a>

## Wasep
Generate a link that will redirect to your Whatsapp number.
<a href="https://github.com/amirulabu/wasep" target="_blank">Source</a>

## Serverless landing page
A simple website that asks the user for name and email. Sometimes called as coming soon page. Powered by Zappa, Flask, and Amazon Dynamodb.
<a href="https://gitlab.com/amirulabu/serverless-landing-page/" target="_blank">Source</a>

## Bitcoin Price Checker
Bitcoin price checker using Python's standard library for GUI, TKinter. Bitcoin price conversion to Ringgit taken from Luno.com.
<a href="https://gist.github.com/amirulabu/7555f19a7f83458840223a7246699604" target="_blank">Source</a>

## Weather applet
A part of freecodecamp projects. Weather applet that uses browser location API.
<a href="https://amirulabu.github.io/demo/weatherapplet/" target="_blank">Demo</a>

## Wikipedia Viewer
A part of freecodecamp projects. Search form using wikipedia API.
<a href="https://amirulabu.github.io/demo/wikipediaviewer/" target="_blank">Demo</a>
