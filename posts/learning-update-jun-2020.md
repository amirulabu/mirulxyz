---
title: "Learning Update: Jun 2020"
description:
date: 2020-06-30
tags:
  - learning
layout: layouts/post.njk
# eleventyExcludeFromCollections: true
---

This is my second month doing learning update, I've learn a lot this month, this month also includes some of the things I started in May and completed in June.

## Coursera: Intro to CS

I joined this course since May 2020, and I found this course a bit different from Havard's CS50. For starters, the course is from Princeton University and it is thought in Java. So I think this would a great exposure to Java since Java is one of my language bucket list. As per any intro to CS MOOC, their grading system for assignments is automated, and quite good. It has been a while since I grind myself doing algorithm questions, but so far so good.

One immediate impression I have for Java is, the language is pretty strict in structuring the program. Each file must contain one class, and very minimum opportunity for programmer to do things their way. Since I come from a JS background, this impression is expected.

## Google Cloud's Cloud Run

I walk through [Jonathan's](https://twitter.com/ernsheong) ebook [Deploying Node.js on GCP](https://gumroad.com/joncloudgeek) and followed the step by step commands to deploy to cloud run. The ebook is pretty tight since it shows all main methods for deploying a nodejs application on GCP and highlights the pros and cons. Here is some of the topics you will learn in the ebook:

- deploy to App Engine
- setup CI/CD (or in simple words, auto-deploy when you push to git master)
- deploy to Cloud Functions
- crash course on dockerizing your application
- deploy to Cloud Run

This is the [result](https://magnoliav2-7zu4pzjkva-uc.a.run.app/).

![magnolia v2 screenshot](/img/magnoliav2screenshot.jpg)