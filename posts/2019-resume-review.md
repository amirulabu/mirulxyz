---
title: 2019 Résumé Review
description:
date: 2020-05-09
tags:
  - resume
layout: layouts/post.njk
# eleventyExcludeFromCollections: true
---

Let us review my 2019 résumé before I quit my job at PetChem. I did this résumé following this very concise guide from [freecodecamp blog](https://www.freecodecamp.org/news/writing-a-killer-software-engineering-resume-b11c91ef699d/).

![résumé 2019](/img/resume2019.jpg)

As I am changing my career from a non-tech to a software engineer, I want to emphasize what can I do by listing my projects first. My advice for Computer Science graduates, better put your qualifications on top, do not follow me.

HabitBot was an idea from my friend, who needs to be disciplined by having a telegram bot remind his everyday schedule. The habits can be added via the web application or by the bot. The bot was taken down since I am not using that bot anymore.

The work process dashboard was one attempt to simplify my daily task at my previous job. I need to track a lot of documents in various stages, therefore I built a web application with a dashboard to easily report to management.

So far both of these project I built using Django, because that time, I just finished the Python Crash Course book and was familiar with Django. Belajar Python Bot, now in its 3rd iteration, previously was using Serverless framework, then I rewrote it using Flask with Zappa and deploy to AWS Lambda. BelajarPythonBot is the longest Python project, that is still up.

I also got some little gigs for doing static websites for short rental houses (homestay). This demonstrates that I already have the basics of HTML, CSS, and JS in place.

Then I continue my résumé with my job experience. I did not put any description on my job at PetChem since it would be not related to any jobs that I was applying to. For the part-time online seller, I put that in, just to show I have that entrepreneurial experience and as a side project, I did while I was in Uni. I also got a side gig to build a website for a printing shop. Surprisingly, the website is still up an alive till today. I occasionally log in to the admin area and update some plugins here and there. The owner of the printing shop still contacts me from time to time. However, I do think the years I work as a web developer is somewhat misleading since I did the website while having an end of semester holiday during Uni. That's why I put the job title as a part-time web developer.

To summarize my skills from my projects above, I list down all the keywords that I deem important to emphasize for the person looking at this resume. My current thoughts today is, I still have a lot to learn about Linux Server Administration, and should not list that there.

Lastly, the education section, where I back up my skills and instill a bit more confidence to the recruiter that I am capable to become a software developer. I completed the CS50 course(and bought the certificate) and did most of Freecodecamp's front-end javascript challenges.

About the web development boot camp, I was lucky since they were just starting up and I am their 2nd or 3rd batch. I also got sponsorship by MAGIC to learn web development, so I joined the class right after I finished my degree in Uni. Although nowadays I do not use Ruby on Rails, through this boot camp I have the confidence to start programming, learn more programming things by myself, and build pet projects.
