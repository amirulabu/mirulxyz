---
title: Do you need serverless?
description:
date: 2021-05-28
tags:
  - serverless
layout: layouts/post.njk
eleventyExcludeFromCollections: true
---

## Scope

- AWS Lambda, serverless framework

## When not to use serverless

### application

- fixed timeout
- public facing endpoints
- application that is using websockets
- quick execution, cannot afford cold start


## When to use serverless

### application

- small team




