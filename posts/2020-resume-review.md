---
title: 2020 Résumé Review
description:
date: 2021-01-28
tags:
  - resume
layout: layouts/post.njk
# eleventyExcludeFromCollections: true
---

I was on a job hunt in early January 2021, so it's time to revise my resume. As per the last resume review, I did this résumé following this very concise guide from the [freecodecamp blog](https://www.freecodecamp.org/news/writing-a-killer-software-engineering-resume-b11c91ef699d/). But this time, I also paid a friend that is quite senior in the IT industry to review my resume. He asked me to highlight more on achievements rather than just work that is done. Follow [the X-Y-Z formula](https://www.inc.com/bill-murphy-jr/google-recruiters-say-these-5-resume-tips-including-x-y-z-formula-will-improve-your-odds-of-getting-hired-at-google.html). 

![resume 2020 part 1](/img/resume2020-1.png)

My friend also advises me to add a summary. Honestly, I did not put much thought into it, I just write up a quick paragraph summarizing my background and what are my strength. It is quite easy for me to know my strength since at Vase we have one-on-one sessions where I can ask for feedback.

Working at Vase or possibly any early to mid-stage startup is recommended if you want to get your hands dirty on multiple things at once. For most of my interview sessions, I can talk a lot about my experience, challenges, what new things I learn, and how I contribute to the company. I am proud of working at Vase and will be forever thankful they took me when I have no professional experience in software development.

Another part that I am excited to share with the interviewer is my projects. I did several pet projects but it is just for experimentation. But then, a friend of mine, a real estate agent told me his problem dealing with leads from Facebook leads, then I pitched him my idea to simplify his workflow. From that, comes the Faizal Property website, where it is a landing page for him to collect leads, with custom CRM. I enjoyed doing the work from planning out the features, to deploying it to production and see it being used. The other projects are a bit old now, I just bring them forward from my 2019 resume.

![resume 2020 part 2](/img/resume2020-2.png)

I added an open-source and talks/activities section after reviewing some online resume samples from resume subreddits and also exchanging resumes with my friends. I believe it demonstrates that I am always open to learning something new and share my knowledge.

On the training and education section, there are not many changes since now I am on the road to be a senior developer and tech lead, so usually, I learn using online resources and also learn on the job.

I maintain the skills section as a catch all keyword section for online submissions, but also a summary of the technologies that I am familiar with.