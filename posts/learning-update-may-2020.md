---
title: "Learning Update: May 2020"
description:
date: 2020-05-26
tags:
  - learning
layout: layouts/post.njk
# eleventyExcludeFromCollections: true
---

Starting this month, I want to record and track all my learnings in this blog. I have been learning so many things, but I did not track or wrote it down systematically in one place. This is my attempt to capture all my learnings or at least what are the subjects that I read or practice.

Since this is the first in this series, I want to record things that I have learned since Jan 2020.

## Serverless framework

I did an endpoint using [serverless framework](http://serverless.com) with NodeJS runtime. It does have quite tight integration with AWS Lambda so deploying to AWS is as easy as running a deploy command.

## Nuxt.js

Nuxt is a Javascript frontend framework using Vue. It does have interesting modes to suit your use cases. Assuming your content is stored in a database, here are some of the modes and use cases:

1. Server Rendered (Universal SSR)

- Loads content in the HTML then served to the client.
- Usually picked to improve SEO.
- Suitable for user-generated content, new content will be available instantly.
- Deploy as a separate service alongside your backend API.

2. Single Page Applications (SPA)

- Loads an empty HTML, then populates the content via Javascript, some web crawlers might have a problem recognizing the content.
- Suitable for user-generated content, new content will be available instantly.
- Deploy as static file, less complexity.

3. Static Generated (Pre Rendering)

- Using `nuxt generate` command to generate all the content in the HTML when deployed.
- Suitable for content that does not change frequently and/or user-generated. Any changes in content need to regenerate the web app.
- Deploy as static file, less complexity.
- Cannot use dynamic routes eg. `example.com/product/:id` ([well you can](https://medium.com/js-dojo/how-i-generated-dynamic-routes-for-different-nuxt-js-pages-ce2ee6972743), but need to keep in mind the changes is only reflected when you regenerate the web app).

## GraphQL subscriptions

Made a simple project to test out GraphQL subscriptions. It is kinda a twitter clone that live updates the feed.
Code is done using VueJS SPA as frontend with Vue Apollo for integrating GraphQL in Vue, and GraphQL Yoga as backend.

https://github.com/amirulabu/twotter, https://twotter.mirul.xyz/

## Strapi CMS

I did a side gig for my friend and decided I do not want to build the backend from scratch using KoaJS, so I tried using Strapi CMS instead. It very fun to work with and I would say Strapi is quite flexible to add functionality. Among additional features I have added for the project:

- ReCaptcha for the contact form
- Facebook login to save long-lived page access token for background tasks
- send telegram message when new content is added.

## Clean Code

I started reading and watching the video series Clean Code, right now I am at chapter 4, Function Structures. I was very glad I started reading Clean Code since there are lots of gems that I can apply for my day to day task as a software engineer. I probably will write a separate blog post on the summary and my opinions on Clean Code.

Now I realize why certain frameworks and libraries write code in a certain way. Why they have certain naming, and how to pick a good name for a class, variable or function.

## Eleventy

This blog is made using Eleventy. It is always a good idea to start with a starter project rather than starting from scratch with any static site generator.
