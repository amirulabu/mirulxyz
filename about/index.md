---
layout: layouts/post.njk
title: About Me
templateClass: tmpl-post
date: 2021-05-22
eleventyNavigation:
  key: About Me
  order: 3
---

I am a person that writes stuff.

## Who are you?

My name is Amirul Abu. Currently working at Popsical as a Mobile Developer. I also do side-projects as a Full Stack Web Developer.

## Why do you like programming?

It feels like playing Lego but instead of following the instructions and follow as per intended design of the kit, you read the library's documentation and build whatever you want out of it.

## What type of programming you do?

I do web development. I am familiar with HTML/CSS. I learn Ruby on Rails in 2014, then self learn Django in 2018. Now I do all my current project using NodeJS. I also do mobile development using Flutter.

## Where is the old website?

[Here](https://web.archive.org/web/20200526074756/https://mirul.xyz/)
